
let http = require("http");

let directory = [
	{
		"name": "Giovanni",
		"email": "giovanni@mail.com"
	},
	{
		"name": "Johari",
		"email": "johari@mail.com"
	}
]

let port = 4000

let server = http.createServer(function (request, response) {

	if (request.url == "/users" && request.method == "GET") {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(JSON.stringify(directory))
		response.end()
	}

	if(request.url == '/users' && request.method == 'POST'){
		// 1. Declare a placeholder variable in order to be re-assigned later on. This variable will be assigned the data inside the body from Postman
		let request_body = ''

		// 2. When data is detected, run a function that assigns that data to the empty request_body variable
		request.on('data', function(data){ //request.on can have either of these three values: data, error, end
			request_body += data
		})

		// 3. Before the request ends, we want to add the new data to our existing users array of objects.
		request.on('end', function(){
			console.log(typeof request_body)

		// 4. Convert the request_body from JSON to a JS Object, then assign that converted value back to the request_body variable.
		request_body = JSON.parse(request_body)

		// 5. Declare a new_user variable signifying the new data that came from the Postman body.
		let new_directory = {
			"name" : request_body.name,
			"email" : request_body.email 
		}

		// 6. Add the new_user variable (object) into the users array.
		directory.push(new_directory)
		console.log(directory)

		// 7. Write the headers for the response. Make sure the content type is 'application/json' since we are writing/returning a JSON
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(JSON.stringify(new_directory))
		response.end()
		})

	}
})

server.listen(port);

console.log('Server running at localhost:4000');